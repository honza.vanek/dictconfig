from typing import Generic, TypeVar, Any, Type, Tuple, NamedTuple, Callable, Sequence, Generator, Optional

T = TypeVar('T')
R = TypeVar('R')


class ConfigKey(str, Generic[T]):
    """Configuration leaf.

    Used in a subclass of Config to define a key-value of given type

    Examples
        >>> class SamplingConfigA(Config):
        >>>      def __init__(self, name: str = '', items: dict = None):
        >>>          self.sample_rate = ConfigKey(int)
        >>>          self.channels = ConfigKey(int)
        >>> #Shorter equivalent:
        >>> class SamplingConfigB(Config):
        >>>      def __init__(self, name: str = '', items: dict = None):
        >>>          self.sample_rate = self.channels = ConfigKey(int)

    Type parameter is optional.

    See :class:`dictconfig.Config`
    """

    def __new__(cls, t: Type[T] = Any, name: str = '', items: dict = None):
        return super(ConfigKey, cls).__new__(cls, name)

    def __init__(self, t: Type[T] = Any, name: str = '', items: dict = None):
        super().__init__()
        self._t = t
        self._items = items

    def __call__(self, default: T) -> T:
        """Gets or sets default and gets value associated with this key in items."""
        if self in self._checked_items():
            return self.get()
        self._items[self] = default
        return default

    def _checked_items(self):
        if self._items is None:
            raise AttributeError('Cannot access items of an uninitialized Config. Initialize it by `with_items()` or `empty()` first.')
        return self._items

    def kv(self, val: T) -> Tuple['ConfigKey[T]', T]:
        """Returns tuple of `(this_key, val)`"""
        return self, val

    def set(self, value: T) -> T:
        """Associates value with this key in underlying items.
        Returns value.
        """
        self._checked_items()[self] = value
        return value

    def get(self) -> T:
        """Returns item associated with this key in items, if present. Otherwise raises Error."""
        return self._checked_items()[self]

    def update(self, f: Callable[[T], T]) -> Optional[T]:
        """If this key has an associated value in items, updates the value to the result of `f(value)`, otherwise sets it to `None`.
        Returns updated value or None.
        """
        return self.set(self.map(f))

    def map(self, f: Callable[[T], R]) -> Optional[R]:
        """If this key has an associated value in items, returns `f(value)`, otherwise returns `None`

        Examples::
          >>> app = AppConfig('app').empty()
          >>> app.server.port.map(bool)
           None
          >>> app.server.port('')
          >>> app.server.port.map(bool)
           False
          >>> app.server.port('1234')
           '1234'
          >>> app.server.port.map(bool)
           True
          >>> app.server.port.map(int)
           1234
        """
        if self._items and self in self._items:
            return f(self.get())

    def or_else(self, supplier: Callable[[], T]) -> T:
        """
        Returns value if set, otherwise uses supplier to calculate, set and return a default value.
        Args:
            supplier: A function which provides a default value

        Returns: Existing or newly created value by supplier.

        """
        if self in self._checked_items():
            return self.get()
        supplied = supplier()
        self._items[self] = supplied
        return supplied

    def or_none(self) -> Optional[T]:
        """Returns value if set, otherwise `None`.

        Returns: Existing value or None.
        """
        if self in self._checked_items():
            return self.get()
        return None


class Config:
    """Configuration node.

    Dictconfig provides a typed, structured (multi-level) view on a flat dict.
    It allows having e.g. your app configuration parameters in a simple,
    easily serializable, flat object, but using it like a structure of nested classes.

    Dict keys are just strings with dot-separated levels, and are interchangeable
    with ConfigKeys.

    Consider following dict::

        cfg = {
            'app.server.port': 8000,
            'app.database.uri': 'mysql:localhost:3306'
        }

    Dict keys are just strings with dot-separated levels, and are interchangeable
    with ConfigKey instances. With corresponding Config/ConfigKey class structure, the dict above
    can be accessed through typed keys as follows::

        app: AppConfig = AppConfig('app').with_items(cfg)

        port = app.server.port.get()
        db_uri = app.database.uri('my-default-uri-if-not-provided')

    Such usage provides code completion and type checking.

    Defined Config keys may be directly used as keys in a dict literal as well,
    because ConfigKey extends str and its value is the whole dict key.
    Notice the absence of quotation marks::

        cfg = {
            app.server.port: 8000,
            app.database.uri: 'mysql:localhost:3306'
        }

    Config classes need to be defined for 'app', 'server', 'database'.
    ConfigKeys are needed for 'port' and 'uri'::

        class ServerConfig(Config):
            __init__(self, name: str = '', items: dict = None):
                self.port = ConfigKey(int)
                super().__init__(name, items)

        class DatabaseConfig(Config):
            __init__(self, name: str = '', items: dict = None):
                self.database = ConfigKey(str)
                super().__init__(name, items)

        class AppConfig(Config):
            __init__(self, name: str = '', items: dict = None):
                self.server = ServerConfig()
                self.database = DatabaseConfig()
                super().__init__(name, items)

    Value modifications by `key.set(newVal)` and `key(defaultVal)` are propagated
    to the backing dict. Because of that, configuration dict may be easily created
    from scratch with the help of type hints::

        app: AppConfig = AppConfig('app').with_items({})
        # or
        app: AppConfig = AppConfig('app').empty()
        app.server.port(8000)
        app.database.uri('mysql:localhost:3306')
    """

    def __init__(self, name: str, items: dict = None):
        self.items: dict = items
        """Underlying `dict` holding configuration parameters"""
        self.name: str = name
        """Name of this Config node"""
        for attr, value in vars(self).items():
            if isinstance(value, (Config, ConfigKey)):
                init_kwargs = value.init_kwargs if hasattr(value, "init_kwargs") else {}
                setattr(self, attr, value.__class__(name=self._child_name(attr), items=items, **init_kwargs))

    def _child_name(self, name):
        return self.name + '.' + name

    def with_items(self, items: dict) -> 'Config':
        """Returns new instance of this Config initialized with items."""
        return self.__class__(self.name, items)

    def empty(self) -> 'Config':
        """Returns new instance of this Config initialized with empty items, ready for manual filling."""
        return self.__class__(self.name, dict())

    def keys(self) -> Generator[ConfigKey, None, None]:
        """Returns generator of all nested :class:`ConfigKey` in this :class:`Config`, recursively. Skips :class:`Config` nodes."""
        for value in vars(self).values():
            if isinstance(value, Config):
                for k in value.keys():
                    yield k
            elif isinstance(value, ConfigKey):
                yield value


class Fun(NamedTuple):
    """Serialization-friendly, poor man's lambda function.

    Designed to be serializable as a simple :class:`dict` with a fully-qualified function reference
    and its named arguments.
    Also supports an argument filter, a list of arguments which will not be passed to this
    function even when passed from the outside.

    For proper JSON serialization, use simplejson library and provide :func:`~dictconfig.encode_call` as
    the default encoder. Predicate :func:`~dictconfig.is_call` may be used when composing multiple
    encoding functions and searching for a compatible encoder.
    Encoded Fun becomes a `dict`, which contains a fully qualified function name. Such dict may be called
    through a :class:`FunKey`, or manually decoded by :func:`~dictconfig.decode_fun`.
    """
    call: Callable
    kwargs: dict
    strip_kwargs: Sequence = []

    def __call__(self, *args, **kwargs):
        """Calls underlying :attr:`call` with union of Fun kwargs and given args/kwargs."""
        kwargs.update(self.kwargs)
        for kw in self.strip_kwargs:
            kwargs.pop(kw, None)
        return self.call(*args, **kwargs)

    def with_args_update(self, **kwargs) -> 'Fun':
        new_kwargs = {**self.kwargs, **kwargs}
        return self.__class__(self.call, new_kwargs, self.strip_kwargs)


def is_call(obj):
    """Predicate returning `True` for callable objects."""
    return callable(obj)


def encode_call(obj):
    """Encoder for JSON serialization of callables.

    Converts a callable to its fully qualified name.
    """
    if not callable(obj):
        raise TypeError(f'Cannot encode {obj} as its fully qualified function name, it is not callable.')

    if hasattr(obj, '__module__') and hasattr(obj, '__qualname__'):
        if obj.__module__ == 'builtins':
            return obj.__qualname__
        return obj.__module__ + '.' + obj.__qualname__

    if hasattr(obj, '__class__'):
        if obj.__class__.__name__ == 'method_descriptor':
            raise TypeError(f'Cannot encode method {obj} as its fully qualified function name. Only functions and class constructors are allowed.')
        else:
            return obj.__class__.__module__ + '.' + obj.__class__.__qualname__

    raise TypeError(f'Cannot encode {obj} as fully qualified function name.')


def decode_call(qualified_name: str) -> Callable:
    """Decodes a callable from its fully qualified name.

    Tries to import the module and return the referenced callable object.
    Supports class constructors and functions.
    """
    [*module_packages, module_name] = qualified_name.split('.')
    from importlib import import_module
    module_path = import_module('.'.join(module_packages))
    if not hasattr(module_path, module_name):
        raise AttributeError(f'Cannot decode {qualified_name} as a Callable. Module "{module_path}" does not have attribute "{module_name}"')
    attr = getattr(module_path, module_name)
    if not callable(attr):
        raise TypeError(f'Cannot decode {qualified_name} as a Callable. Object "{module_path}.{module_name}" is not callable')
    return attr


def decode_fun(fun: dict or Fun) -> Fun:
    """Re-create a Fun from its dict form, e.g. like this::
        {"call": "FunClass", "kwargs": {"aaa": 5, "be": "str"}, "strip_kwargs": ["not_this"]}}
    """
    if isinstance(fun, Fun):
        return fun
    if 'call' not in fun:
        raise KeyError(f'Cannot create Fun from {fun}. Key "call" is missing.')
    call = decode_call(fun['call'])
    all_kwargs = {k: v for k, v in fun.items() if k != 'call'}
    return Fun(call=call, **all_kwargs)


class FunKey(ConfigKey[Fun]):
    """Special case of a ConfigKey for callables in configurations.

    Holds :class:`Fun` as the value, or its dict representation.
    Fun may be called by :func:`Fun.call`
    """

    def __new__(cls, name: str = '', items: dict = None):
        return super(FunKey, cls).__new__(cls, Fun, name)

    def __init__(self, name: str = '', items: dict = None):
        super().__init__(Fun, name, items)

    def get(self) -> Fun:
        """Returns underlying :class:`Fun` in config items.

        When configuration contains Fun in the form of a (deserialized) dict,
        it tries to import function referenced in :meth:`Fun.call` and replace the dict
        with a Fun instance, before calling it.
        """
        item = super().get()
        if isinstance(item, Fun):
            return item
        if isinstance(item, dict):
            decoded_fun = decode_fun(item)
            self.set(decoded_fun)
            return decoded_fun
        raise TypeError(f'Fun {item} is nor an instance of Fun nor a dict representing a Fun.')
