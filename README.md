# Installation

## From master branch: 
```
pip install git+https://gitlab.com/honza.vanek/dictconfig.git
```

## From specific branch: 
```
pip install git+https://gitlab.com/honza.vanek/dictconfig.git@packaging
```
