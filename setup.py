import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

__version__ = '0.0.1'

setuptools.setup(
    name='dictconfig',
    version=__version__,
    author='Honza Vanek',
    description='dictconfig',
    license='BSD 3-Clause License',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/honza.vanek/dictconfig',
    package_dir={'': 'src'},
    packages=setuptools.find_packages("src"),
    install_requires=['setuptools'],
)
